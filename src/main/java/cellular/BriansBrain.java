package cellular;

import java.util.Random;

import javax.lang.model.util.ElementScanner14;
import javax.management.relation.RoleNotFoundException;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;
    
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }
    
    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
                //BriansBrain has dying to
                int random_color = random.nextInt(3);
                if (random_color == 0) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                }
                else if(random_color == 1) {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
				else {
					currentGeneration.set(row, col, CellState.DYING);
				}
			}
		}
    }


    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }


    @Override
    public CellState getCellState(int row, int column) {
        return this.currentGeneration.get(row, column);
    }


    @Override
    public void step() {
        IGrid nextGenereation = currentGeneration.copy();

        for(int i = 0; i < numberOfRows(); i ++) {
            for(int j = 0; j < numberOfColumns(); j ++) {
                nextGenereation.set(i, j, getCellState(i, j));
            }
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState newState = currentGeneration.get(row, col);
        int neighborState = countNeighbors(row, col, CellState.ALIVE);
        
        if (newState.equals(CellState.ALIVE)) {
            newState = CellState.DYING;
        }
        else if (newState.equals(CellState.DYING)) {
            newState = CellState.DEAD;
        }
        else {
            if (newState.equals(CellState.DEAD) & neighborState == 2) {
                newState = CellState.ALIVE;
            }   
        }
        return newState;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int count = 0;

        for(int i = row-1; i <= row+1; i ++){
            for (int j = col-1; j <= col+1; j ++){
                if (i < 0 || i >= currentGeneration.numRows() || j < 0 || j >= currentGeneration.numColumns()) {
                    count +=0;
                }
                else if(i == row && j == col) {
                    count += 0;
                }
                else {
                    if (state.equals(CellState.DEAD) || state.equals(CellState.DYING)) {
                        if (currentGeneration.get(i, j).equals(CellState.ALIVE)) {
                            count ++;
                        }
                    }
                    else if(state.equals(CellState.ALIVE)) {
                        if (currentGeneration.get(i, j).equals(state)) {
                            count ++;
                        }
                    }
                }
            }
        }

        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    
}
