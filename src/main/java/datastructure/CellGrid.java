package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int columns;
    ArrayList<CellState> state;

    public CellGrid(int rows, int columns, CellState initialState) {
		if (rows >= 0 && columns >= 0){
            this.rows = rows;
            this.columns = columns;
            this.state = new ArrayList<CellState>();

            int numEntries = rows*columns;
            for (int i = 0; i < numEntries; i++) {
                state.add(initialState);
            }
        }    
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        int index = getIndex(row, column);
        state.set(index, element);
        
    }

    private int getIndex(int row, int col) {
        if (row >= this.numRows() || col >= this.numColumns()) {
            throw new IndexOutOfBoundsException("Too high Index");
        }
        if (row < 0 || col < 0) {
            throw new IndexOutOfBoundsException("Too low Index");
        }
        return row*this.columns + col;
    }
    

    @Override
    public CellState get(int row, int column) {
        int index = getIndex(row, column);
        return this.state.get(index);
    }

    @Override
    public IGrid copy() {
        CellGrid newgGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col< this.columns; col++) {
                CellState value = this.get(row, col);
                newgGrid.set(row, col, value);
            }
        }
        return newgGrid;
    }
    
}
